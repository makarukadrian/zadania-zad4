# -*- coding: utf-8 -*-

import unittest
import httplib
from funkload.FunkLoadTestCase import FunkLoadTestCase

class Test(FunkLoadTestCase):

	def setUp(self):
        self.server_url = self.conf_get('main', 'url')
		
		
    
    def test_post(self):
        resp = self.post(self.server_url + "/", ok_codes=[405], params=[], description="POST 405")
        self.assert_(resp.code in [405], "expecting a 405")
    def test_get(self):
        resp = self.get(self.server_url+'/tiru', ok_codes=[404], description='GET 404')
        self.assert_(resp.code in [404], "expecting a 404")
	def test_jpeg(self):
        resp = self.get(self.server_url + '/images/jpg_rip.jpg', description='GET JPG')
        self.assert_(resp.code in [200], "expecting a 200")
    def test_txt(self):
         resp = self.get(self.server_url + '/lokomotywa.txt', description='GET TXT')
        self.assert_(resp.code in [200], "expecting a 200")
    def test_html(self):
       resp = self.get(self.server_url + '/web_page.html', description='GET HTML')
        self.assert_(resp.code in [200], "expecting a 200")
    def test_dir(self):
        resp = self.get(self.server_url + '/', description='GET DIR')
        self.assert_(resp.code in [200], "expecting a 200")



if __name__ == '__main__':
    unittest.main()