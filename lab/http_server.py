# -*- coding: utf-8 -*-

import socket
import os
import mimetypes
from email.utils import formatdate
import logging
import time
from daemon import runner

def create_server():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server_address = ('194.29.175.240', 12233)
    s.bind(server_address)
    s.listen(3)

    try:
        while True:
            print('Server is running, awaiting connection')
            conn, addr = s.accept()
            print('Connected with {0}:{1}'.format(*addr))

            try:
                request = conn.recv(1024)
                ans = create(request)
                conn.sendall(ans)
            except:
                serverERR()
            finally:
                conn.close()

    finally:
        socket.close()

def connect(msg, getORhttp, html):
    header = 'HTTP/1.1 ' + msg + rn()
    contnent_type = 'Content-Type: '+ getORhttp + rn()
    data = 'GMT-Date: '+ email.utils.formatdate()+ rn()
    length = 'Content_Length: '+ str(len(html))+ rn()
    return header + contnent_type + data + length + rn() + html

def rn():
    return '\r\n'

def create(request):
    try:
        splitted = request.split(rn())
        getORhttp = splitted[0].split(' ')[0]
        path = '/home/p11/zad4/lab/web/' + (splitted[0].split(' ')[1])[1:]

        if(getORhttp == 'GET' or getORhttp == 'HTTP'):
            if os.path.isdir(path):
                web = head() + '</head><body><h1>' + path + ':</h1>'
                if(path == '/home/p11/zad4/lab/web/'):
                    charr = ''
                else:
                    charr = '/'
                for files in os.listdir(path):
                    web += '<li><a href="' + charr + path.replace('web/', "") + '/' + files + '">' + files + '</a></li></body></html>'
                return connect('200 OK', 'text/html', web)
            else:
                if(os.path.exists(path)):
                    return connect('200 OK', mimetypes.guess_type(path)[0], open(path, 'rb').read())
                else:
                    error = '404 Not Found'
                    return connect(error, 'text/html', returnERR(error))
        else:
            error = '405 Method Not Allowed'
            return connect(error, 'text/html', returnERR(error))
    except:
        serverERR()

def serverERR():
    error = '500 Internal Server Error'
    return connect(error, 'text/html', returnERR(error))

def returnERR(tresc):
    return head() + '</head><body><h1>' + tresc + '</h1></body></html>'

def head():
    return '<!DOCTYPE html><html><head><meta charset="utf-8" />'

class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/demon.pid'
        self.pidfile_timeout = 5

    # Główny kod aplikacji
    def run(self):
        create_server()


# Ustawienie loggera
logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/home/p11/zad4/lab/log/demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)


if __name__ == '__main__':
    # Stworzenie i uruchomienie aplikacji jako demona
    app = App()
    daemon_runner = runner.DaemonRunner(app)
    # Zapobiegnięcie zamknięciu pliku logów podczas demonizacji
    daemon_runner.daemon_context.files_preserve = [handler.stream]
    daemon_runner.do_action()