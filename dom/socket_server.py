

import http_server

from twisted.internet import protocol, reactor
from twisted.web import resource, server



class HelloResource(resource.Resource):
    isLeaf = True
    numberRequests = 0

    def render_GET(self, request):
        self.numberRequests += 1
        print "I am request #" + str(self.numberRequests) + "\n"
        return http_server.create(request)

if __name__ == '__main__':
    reactor.listenTCP(12233, server.Site(HelloResource()))
    reactor.run()
