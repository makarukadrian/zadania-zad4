import unittest
import httplib
from funkload.FunkLoadTestCase import FunkLoadTestCase

class Test(FunkLoadTestCase):

    def Ust(self):
        self.server_url = self.conf_get('main', 'url')

    def get(self):
        server_url = self.server_url
        nb_time = self.conf_getInt('test_get', 'nb_time')
        for i in range(nb_time):
            r = self.get(server_url, description='Get URL')
            self.assert_(r.code == 200, "expecting a 200")

if __name__ == '__main__':
    unittest.main()