# -*- coding: utf-8 -*-

import socket
import os
import mimetypes


def create_server(s):
    try:
        while True:
            print('Server is running, awaiting connection')
            conn, addr = s.accept()
            print('Connected with {0}:{1}'.format(*addr))

            try:
                request = conn.recv(1024)
                ans = create(request)
                conn.sendall(ans)
            except:
                serverERR()
            finally:
                conn.close()

    finally:
        socket.close()


def connect(msg, getORhttp, html, request):
    #header = 'HTTP/1.1 ' + msg + rn()
    #contnent_type = 'Content-Type: ' + getORhttp + rn()
    #length = 'Content_Length: ' + str(len(html)) + rn()
    request.setHeader("Content-Type", getORhttp + "; charset=UTF-8")
    request.setHeader("Content-Length", str(len(html)))

    #return header + contnent_type + length + rn() + html
    return html


def rn():
    return '\r\n'


def create(request):

    splitted = str(request).split(rn())
    getORhttp = splitted[0].split(' ')[0]
    path = 'web/' + str(request.uri).lstrip('/')
    print getORhttp, path, splitted 

    if ('GET' in str(request) and 'HTTP' in str(request)):
        if os.path.isdir(path):
            web = head() + '</head><body><h1>' + path + ':</h1>'
            if (path == 'web/'):
                charr = ''
            else:
                charr = '/'
            for files in os.listdir(path):
                web += '<li><a href="' + charr + path.replace('web/',
                                                              "") + '/' + files + '">' + files + '</a></li></body></html>'
            request.setResponseCode(200)
            return connect('200 OK', 'text/html', web, request)
        else:
            if (os.path.exists(path)):
                request.setResponseCode(200)
                return connect('200 OK', mimetypes.guess_type(path)[0], open(path, 'rb').read(), request)
            else:
                request.setResponseCode(404)
                error = '404 Not Found'
                return connect(error, 'text/html', returnERR(error), request)
    else:
        request.setResponseCode(405)
        error = '405 Method Not Allowed'
        return connect(error, 'text/html', returnERR(error), request)


def serverERR():
    error = '500 Internal Server Error'
    return connect(error, 'text/html', returnERR(error))


def returnERR(tresc):
    return head() + '</head><body><h1>' + tresc + '</h1></body></html>'


def head():
    return '<!DOCTYPE html><html><head><meta charset="utf-8" />'
